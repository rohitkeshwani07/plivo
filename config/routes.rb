Rails.application.routes.draw do
  get 'v1/contacts', to: 'contact#index'
  get 'v1/contacts/:id', to: 'contact#show'
  post 'v1/contacts/:id', to: 'contact#update'
  put 'v1/contacts', to: 'contact#create'
end
