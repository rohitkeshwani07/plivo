require "rails_helper"

RSpec.describe ContactController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/v1/contacts").to route_to("contact#index")
    end

    it "routes to #show" do
      expect(:get => "/v1/contacts/1").to route_to("contact#show", :id => "1")
    end

    it "routes to #create" do
      expect(:put => "/v1/contacts").to route_to("contact#create")
    end

    it "routes to #update via PUT" do
      expect(:post => "/v1/contacts/1").to route_to("contact#update", :id => "1")
    end
  end
end
