require 'rails_helper'

RSpec.describe "Contacts", type: :request do
  describe "PUT /contacts" do
    it "works! (now write some real specs)" do
      put "/v1/contacts", params: { name: "xyz", email: "20.2"}
      expect(response).to have_http_status(200)
    end
  end
  describe "GET /contacts" do
    it "works! (now write some real specs)" do
      get "/v1/contacts"
      expect(response).to have_http_status(200)
    end
  end
end
