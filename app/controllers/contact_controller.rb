class ContactController < ApplicationController
  skip_before_action :verify_authenticity_token
  protect_from_forgery :except => [:update, :create]
  def index
  	contacts = Contact.where("name LIKE ?", "%#{params[:name]}%")
	  	.where("name LIKE ?", "%#{params[:email]}%")
	  	.paginate(page: params[:page], per_page: 10)
  	render json: contacts || [], :status => 200
  end
  def show
  	render json: (Contact.find(params[:id]) || {}).to_json, :status => 200
  end
  def create
  	begin
  	  contact = Contact.where(email: params['email'])
  	rescue ActiveRecord::RecordNotFound => e
  	  contact = nil
  	end
  	if (contact == nil or contact.length == 0)
  		contact = Contact.new(contacts_params)
  		contact.save
      render json: contact || {}, :status => 200
  	else
  		render json: {"error_message": "Contact already exist."}, :status => 400
  	end
  end
  def update
  	byebug
  	begin
	  contact = Contact.find(params[:id])
	rescue ActiveRecord::RecordNotFound => e
	  contact = nil
	end
	if (contact != nil)
		contact.name = params[:name]
		contact.email = params[:email]
	  	contact.save
  	else
  		render json: {"error_message": "Contact doesn't exist."}, :status => 400
  	end
  end
  private
    def contacts_params
      params.permit(:name, :email)
    end
end
